#import "Kiwi.h"
#import "EDTimerService.h"
#import "EDTimer.h"

SPEC_BEGIN(TimerServiceSpec)

    __block EDTimerService *timerService;
    beforeEach(^{
        timerService = [EDTimerService new];
        [timerService addTimerAndCall:nil]; //now there are two
        [timerService.timers enumerateObjectsUsingBlock:^(EDTimer *obj, NSUInteger idx, BOOL *stop) {
            obj.timeInterval = 1000;
        }];
    });

    afterEach(^{
        timerService = nil;
    });

    describe(@"starting with all valid timers", ^{
        it(@"current timer has to be set", ^{
            [timerService start];
            [timerService.currentTimer shouldNotBeNil];
        });

        it(@"all timers zero, should not start", ^{
            [timerService deleteTimers];
            BOOL started = [timerService start];
            [[theValue(started) should] equal:theValue(NO)];
            [timerService.currentTimer shouldBeNil];
        });
        it(@"starting when timer alredy running. Should throw exception", ^{
            [[theBlock(^{
                [timerService start];
                [timerService start]; //this should throw an exception
            }) should] raise];
        });
    });

    describe(@"starting next, all valid timers", ^{
        beforeEach(^{
            [timerService start];
        });

        it(@"there should be two timers", ^{
            [[theValue(timerService.timers.count) should] equal:theValue(2)];
        });

        it(@"current timer should be next", ^{
            EDTimer *nextTimer = (timerService.timers)[1];
            [timerService startNext];
            [[theValue(nextTimer) should] equal:theValue(timerService.currentTimer)];
        });

        it(@"current timer has to be set", ^{
            [timerService startNext];
            [timerService.currentTimer shouldNotBeNil];
        });

        it(@"next on last timer should return NO", ^{
            [timerService startNext];
            BOOL result = [timerService startNext];
            [[theValue(result) should] equal:theValue(NO)];
        });
        it(@"all timers should have stopDate", ^{
            [timerService startNext];
            [timerService startNext];
            [timerService startNext];
            [timerService.timers enumerateObjectsUsingBlock:^(EDTimer *timer, NSUInteger idx, BOOL *stop) {
                [[timer.stopDate shouldNot] beNil];
            }];
        });

    });

    describe(@"starting when first timer zero", ^{
        beforeEach(^{
            EDTimer *firstTimer = timerService.timers[0];
            firstTimer.timeInterval = 0;
        });
        it(@"start first -> second should run", ^{
            BOOL started = [timerService start];
            [[theValue(started) should] equal:theValue(YES)];
            [[timerService.currentTimer should] equal:timerService.timers[1]];
        });
    });

    describe(@"starting all zero timers", ^{
        beforeEach(^{
            [timerService.timers enumerateObjectsUsingBlock:^(EDTimer *obj, NSUInteger idx, BOOL *stop) {
                obj.timeInterval = 0;
            }];
        });
        it(@"should not start", ^{
            BOOL result = [timerService start];
            [[theValue(result) should] equal:theValue(NO)];
        });
    });

    describe(@"getting previous valid timer", ^{
        it(@"getting previous valid when first is runnign -> should be nil", ^{
           [timerService start];
            EDTimer *previousTimer = [timerService getPreviousValid];
            [[previousTimer should] beNil];
        });
        it(@"getting previous valid when second (from two valid) timer is running -> should return first", ^{
            [timerService start];
            [timerService startNext];

            EDTimer *previousValidTimer = [timerService getPreviousValid];
            [[previousValidTimer should] beNonNil];
            [[previousValidTimer should] equal:timerService.timers[0]];
        });
        it(@"getting previous valid with 'gap'", ^{
            [timerService addTimerAndCall:nil]; //now there are three
            EDTimer *secondZeroTimer = timerService.timers[1];
            secondZeroTimer.timeInterval = 0;
            EDTimer *lastNotZeroTimer = timerService.timers[2];
            lastNotZeroTimer.timeInterval = 1000;

            [timerService start];
            [timerService startNext];
            [[lastNotZeroTimer should] equal:timerService.currentTimer];

            EDTimer *previousValid = [timerService getPreviousValid];
            [[timerService.timers[0] should] equal: previousValid];
        });
    });

    describe(@"reverting timers", ^{
        it(@"First timer isn't revertable", ^{
            [timerService start];
            BOOL revertable = timerService.revertable;
            [[theValue(revertable) should] equal:theValue(NO)];
        });
        it(@"second timer is revertable", ^{
            [timerService start];
            [timerService startNext];
            BOOL revertable = timerService.revertable;
            [[theValue(revertable) should] equal:theValue(YES)];
        });
        void (^startStartNext)(void)  = ^{
            [timerService start];
            [timerService startNext];
        };
        it(@"reverting timer - current should be the previous", ^{
            startStartNext();
            __block BOOL updateBlockWasCalled = NO;
            [timerService revertAndCall:^(int timerIndex, NSTimeInterval startValue) {
                updateBlockWasCalled = YES;
            }];
            EDTimer *currentTimer = timerService.currentTimer;
            EDTimer *firstTimer = timerService.timers[0];
            [[currentTimer should] equal:firstTimer];
            [[theValue(updateBlockWasCalled) should] equal:theValue(YES)];
        });
        it(@"reverting timer with gap - current should be previous", ^{
            [timerService addTimerAndCall:nil]; //now there are three
            EDTimer *secondZeroTimer = timerService.timers[1];
            secondZeroTimer.timeInterval = 0;
            EDTimer *lastNotZeroTimer = timerService.timers[2];
            lastNotZeroTimer.timeInterval = 1000;
            startStartNext();

            [[timerService.timers[2] should] equal:timerService.currentTimer];  //the third timer should be running

            __block int callBackTimerIndex = -1;
            [timerService revertAndCall:^(int timerIndex, NSTimeInterval startValue) {
                callBackTimerIndex = timerIndex;
            }];

            [[timerService.currentTimer should] equal: timerService.timers[0]];
            [[theValue(callBackTimerIndex) should] equal:theValue(0)];
        });
    });

SPEC_END