#import "Kiwi.h"
#import "EDTimerListController.h"
#import "EDTimerListDataSource.h"


SPEC_BEGIN(TimerListControllerSpec)
    __block EDTimerListController *timerListController;

    beforeEach(^{
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
        timerListController = [storyboard instantiateViewControllerWithIdentifier:@"TimerListController"];
    });

    describe(@"Math", ^{
        it(@"is pretty cool", ^{
            NSUInteger a = 16;
            NSUInteger b = 26;
            [[theValue(a + b) should] equal:theValue(42)];
        });
    });



    describe(@"clearing timers", ^{
        __block EDTimerListDataSource *dataSource;
        beforeEach(^{
            dataSource = timerListController.dataSource;
            NSLog(@"clearing timers - beforeEach");
            //[timerListController.dataSource deleteAll];
            [timerListController addTimer:nil];
            [timerListController addTimer:nil];
        });
        it(@"clearing timers", ^{

            //[[theValue(dataSource.timers.count) should] equal:@3];

        });
    });

SPEC_END