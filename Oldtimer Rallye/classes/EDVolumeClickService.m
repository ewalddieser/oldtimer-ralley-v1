#import "EDVolumeClickService.h"
#import "VolumeButtonDelegate.h"
#import "RBVolumeButtons.h"

@interface EDVolumeClickService ()
@property(strong) RBVolumeButtons *buttonStealer;
@end


@implementation EDVolumeClickService {

}

+ (EDVolumeClickService *)sharedInstance {
    static EDVolumeClickService *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[EDVolumeClickService alloc] init];
    });
    return sharedInstance;
}

- (EDVolumeClickService *)init {
    if (self = [super init]) {
        self.buttonStealer = [[RBVolumeButtons alloc] init];
        __weak typeof(self) weakSelf = self;
        self.buttonStealer.upBlock = ^{
            [weakSelf.delegate volumeUp];
        };
        self.buttonStealer.downBlock = ^{
            [weakSelf.delegate volumeDown];
        };
        //[self.buttonStealer startStealingVolumeButtonEvents];
    }
    return self;
}

- (void)setDelegate:(id <VolumeButtonDelegate>)delegate {
    _delegate = delegate;
    if (delegate == nil) {
        [self.buttonStealer stopStealingVolumeButtonEvents];
    } else {
        [self.buttonStealer startStealingVolumeButtonEvents];
    }
}

@end