#import <Foundation/Foundation.h>

@interface EDClicker : NSObject

- (void)click;

- (void)zeroClick;

@end