#import <Foundation/Foundation.h>

@interface EDTimer : NSObject

@property(nonatomic) NSTimeInterval timeInterval;

@property(nonatomic) int index;

@property(strong, nonatomic) NSDate *startDate;    //This is the actual time when the timer was started
@property(strong, nonatomic) NSDate *stopDate;     //This is the actual time when the timer was stopped

- (void)start;

- (void)stop;

- (void)reset;

/**
* If timer gets stopped unintentionally and has to continue.
*/
- (void)continueCounting;

- (NSString *)strVal;

+ (NSString *)strVal:(int)timeVal;

@end