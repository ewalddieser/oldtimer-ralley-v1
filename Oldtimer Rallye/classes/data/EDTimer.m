#import "EDTimer.h"

@implementation EDTimer

- (NSTimeInterval)missedTime {
    if (_stopDate == nil) {
        return 0;
    }
    //The actually stopped time
    NSTimeInterval stoppedTime = [_stopDate timeIntervalSinceDate:_startDate];
    NSTimeInterval missedTime = _timeInterval - stoppedTime;
    return missedTime;
}

- (void)start {
    _startDate = [NSDate new];
}

- (void)stop {
    if(self.timeInterval == 0)
        return;
    _stopDate = [NSDate new];
}


//FIXME really needed?
- (void)reset {
    _startDate = nil;
    _stopDate = nil;
}

- (void)continueCounting {
    _stopDate = nil;
}

- (NSString *)strVal {
    NSTimeInterval time = _timeInterval;;
    NSString *vz = @"";
    if (_stopDate != nil) {
        time = [self missedTime];
        if (time < 0) {
            vz = @"+";
        } else {
            vz = @"-";
        }
    }
    time = fabs(time);
    int hours = (int) floor(time / (60 * 60));
    int minutes = (int) floor((time / 60) - hours * 60);
    int seconds = (int) floor(time - (minutes * 60) - (hours * 60 * 60));

    int allSeconds = (int) floor(time);
    int millis = (int) floor((time - allSeconds) * 100);  // nicht wirklich Millisekunden, 10tel und 100tel

    NSString *ret;

    if ([@"" isEqualToString:vz]) {
        ret = [NSString stringWithFormat:@"%@ : %@ : %@", [EDTimer strVal:hours], [EDTimer strVal:minutes], [EDTimer strVal:seconds]];
    } else {
        ret = [NSString stringWithFormat:@"%@%@:%@:%@,%@", vz, [EDTimer strVal:hours], [EDTimer strVal:minutes], [EDTimer strVal:seconds], [EDTimer strVal:millis]];
    }
    return ret;
}

+ (NSString *)strVal:(int)timeVal {
    if (timeVal < 10) {
        return [NSString stringWithFormat:@"0%d", timeVal];
    }
    return [NSString stringWithFormat:@"%d", timeVal];
}


@end