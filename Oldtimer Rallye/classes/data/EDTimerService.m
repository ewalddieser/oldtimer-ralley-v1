#import "EDTimerService.h"
#import "EDTimer.h"

@interface EDTimerService ()
@property(strong, nonatomic) NSUserDefaults *userDefaults;
@end


@implementation EDTimerService {
    EDTimer *firstValidTimer;
}

- (EDTimerService *)init {
    if (self = [super init]) {
        _userDefaults = [NSUserDefaults standardUserDefaults];
        [self updateTimersFromDefaults];
    }
    return self;
}

- (EDTimerService *)initWithUserDefaults:(NSUserDefaults *)userDefaults {
    if (self = [super init]) {
        _userDefaults = userDefaults;
        [self updateTimersFromDefaults];
    }
    return self;
}

- (void)updateTimersFromDefaults {
    NSUserDefaults *defaults = self.userDefaults;
    NSArray *persistedTimers = [defaults objectForKey:@"timers"];
    if (persistedTimers == nil) {
        NSTimeInterval initialTimeInterval = 0;
        persistedTimers = @[@(initialTimeInterval)];
        [defaults setObject:persistedTimers forKey:@"timers"];
        [defaults synchronize];
    }
    _timers = [self timersFromArray:persistedTimers];
}

+ (EDTimerService *)sharedInstance {
    static EDTimerService *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[EDTimerService alloc] init];
    });
    return sharedInstance;
}

- (void)addTimerAndCall:(void (^)(void))callBack {
    if (_timers.count >= 99) return;  // 99 Timers is the maximum

    EDTimer *timer = [EDTimer new];
    timer.index = _timers.count + 1;

    [_timers addObject:timer];

    if (callBack != nil) {
        callBack();
    }
}

- (BOOL)last {
    return [[_timers lastObject] isEqual:_currentTimer];
}

- (BOOL)start {
    if (_currentTimer != nil) {
        @throw([NSException exceptionWithName:@"InvalidState"
                                       reason:@"Timer already started!"
                                     userInfo:nil]);
    }
    firstValidTimer = [self getFirstNotNull];
    if (firstValidTimer) {
        _currentTimer = firstValidTimer;
        [_currentTimer start];
        return YES;
    }
    return NO;
}

- (BOOL)startNext {
    if (_currentTimer == nil) {
        return NO;
    }
    [_currentTimer stop];
    if (self.last) {
        _currentTimer = nil;
        return NO;
    }
    NSUInteger nextTimerIndex = [_timers indexOfObject:_currentTimer] + 1;
    _currentTimer = _timers[nextTimerIndex];
    if (_currentTimer.timeInterval == 0) {
        return [self startNext];
    }
    [_currentTimer start];
    return YES;
}

/**
* This is used to find out if revert button in the ui is on/off
*/
- (BOOL)revertable {
    return ![self.currentTimer isEqual:firstValidTimer];
}

- (EDTimer *)getPreviousValid {
    if (self.currentTimer == firstValidTimer) {
        return nil;
    }
    unsigned int currentIndex = [self.timers indexOfObject:self.currentTimer];
    EDTimer *previousValidTimer = nil;
    do {
        currentIndex = currentIndex - 1;
        EDTimer *testTimer = self.timers[currentIndex];
        if (testTimer.timeInterval > 0) {
            previousValidTimer = testTimer;
        }
    } while (previousValidTimer == nil && currentIndex >= 1);
    return previousValidTimer;
}

- (BOOL)revertAndCall:(void (^)(int timerIndex, NSTimeInterval startValue))uiUpdateBlock {
    if (![self revertable]) {
        return NO; // FIXME better throw exception? What is the suggested best practice?
    }
    self.currentTimer = [self getPreviousValid];
    self.currentTimer.stopDate = nil;

    int currentTimerIndex = [self.timers indexOfObject:self.currentTimer];
    double timeCurrentTimer = self.currentTimer.startDate.timeIntervalSince1970 + self.currentTimer.timeInterval;
    double timeNow = [NSDate new].timeIntervalSince1970;
    double deltaInterval = timeNow - timeCurrentTimer;

    uiUpdateBlock(currentTimerIndex, deltaInterval);
    return YES;
}

/**
* All timers are deleted. One initial timer "00:00:00" is added.
*/
- (NSMutableArray *)deleteTimers {
    NSTimeInterval initialTimeInterval = 0;
    NSArray *persistedTimers = @[@(initialTimeInterval)];
    [self.userDefaults setObject:persistedTimers forKey:@"timers"];
    [self.userDefaults synchronize];
    [_timers removeAllObjects];
    [_timers addObjectsFromArray:[self timersFromArray:persistedTimers]];
    _currentTimer = nil;
    return _timers;
}

- (NSMutableArray *)resetTimers {
    [self persistTimers];
    NSArray *persistedTimers = [self.userDefaults objectForKey:@"timers"];
    NSMutableArray *restoredTimers = [self timersFromArray:persistedTimers];
    [_timers removeAllObjects];
    [_timers addObjectsFromArray:restoredTimers];
    return _timers;
}

/**
* Creating an Array of EDTimers from persisted timer values
*/
- (NSMutableArray *)timersFromArray:(NSArray *)persistedTimers {
    NSMutableArray *array = [NSMutableArray new];
    int index = 1;
    for (NSNumber *initialVal in persistedTimers) {
        NSTimeInterval interval = [initialVal doubleValue];
        EDTimer *timer = [EDTimer new];
        timer.timeInterval = interval;
        timer.index = index;
        index++;
        [array addObject:timer];
    }
    return array;
}

- (NSUInteger)currentTimerIndex {
    return [self.timers indexOfObject:self.currentTimer];
}

- (void)persistTimers {
    NSMutableArray *timersToPersist = [NSMutableArray new];
    for (EDTimer *timer in _timers) {
        [timersToPersist addObject:@(timer.timeInterval)];
    }
    [self.userDefaults setObject:[NSArray arrayWithArray:timersToPersist] forKey:@"timers"];
    [self.userDefaults synchronize];
}

- (EDTimer *)getFirstNotNull {
    for (EDTimer *timer in _timers) {
        if (timer.timeInterval > 0) {
            return timer;
        }
    }
    return nil;
}

@end