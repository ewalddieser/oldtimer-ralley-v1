#import <Foundation/Foundation.h>

@class EDTimer;


@interface EDTimerService : NSObject

@property(strong, nonatomic) EDTimer *currentTimer;
@property(strong, nonatomic) NSMutableArray *timers;
@property(readonly) BOOL last;
@property(readonly) BOOL revertable;

- (EDTimerService *)initWithUserDefaults:(NSUserDefaults *)userDefaults;

- (NSMutableArray *)deleteTimers;

- (NSMutableArray *)resetTimers;

- (BOOL)start;

- (BOOL)startNext;

- (EDTimer *)getPreviousValid;

- (BOOL)revertAndCall:(void (^)(int timerIndex, NSTimeInterval startValue))uiUpdateBlock;

- (void)persistTimers;

- (void)addTimerAndCall:(void (^)(void))callBack;

- (EDTimer *)getFirstNotNull;

- (NSUInteger)currentTimerIndex;

+ (EDTimerService *)sharedInstance;

@end