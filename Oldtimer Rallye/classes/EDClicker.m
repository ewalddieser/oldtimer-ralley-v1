#import <AVFoundation/AVFoundation.h>
#import "EDClicker.h"

@interface EDClicker () {
    SystemSoundID clickSound;
    SystemSoundID zeroClickSound;
}
@end

@implementation EDClicker

- (id)init {
    if (self = [super init]) {
        [self setupClick];
        [self setupZeroClick];
    }
    return self;
}

- (void)setupClick {
    NSString *soundPath = [[NSBundle mainBundle] pathForResource:@"beep" ofType:@"wav"];
    NSURL *soundURL = [NSURL fileURLWithPath:soundPath];
    AudioServicesCreateSystemSoundID((__bridge CFURLRef) soundURL, &clickSound);
}

- (void)setupZeroClick {
    NSString *soundPath = [[NSBundle mainBundle] pathForResource:@"zeroBeep" ofType:@"wav"];
    NSURL *buttonURL = [NSURL fileURLWithPath:soundPath];
    AudioServicesCreateSystemSoundID((__bridge CFURLRef) buttonURL, &zeroClickSound);
}

#pragma mark - public methods

- (void)click {
    AudioServicesPlaySystemSound(clickSound);
}

- (void)zeroClick {
    AudioServicesPlaySystemSound(zeroClickSound);
}

@end