#import "EDTimerListDataSource.h"
#import "EDTimer.h"
#import "EDTimerService.h"

@interface EDTimerListDataSource ()
@property(nonatomic, weak) EDTimerService *timerService;
@end

@implementation EDTimerListDataSource

- (id)init {
    self = [super init];
    if (self != nil) {
        _timers = EDTimerService.sharedInstance.timers;
    }
    return self;
}

- (EDTimerService *) timerService {
    if(_timerService == nil) {
        _timerService = EDTimerService.sharedInstance;
    }
    return _timerService;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _timers.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    EDTimer *timer = _timers[(NSUInteger) indexPath.row];
    UITableViewCell *cell;
    if (timer.stopDate != nil) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"resultTimeCell"];
    }else {
        cell = [tableView dequeueReusableCellWithIdentifier:@"stdTimeCell"];
    }

    [cell setBackgroundColor:[UIColor clearColor]];

    UILabel *timerLabel = (UILabel *) [cell viewWithTag:111];
    timerLabel.text = timer.strVal;
    UILabel *timerNr = (UILabel *) [cell viewWithTag:222];
    timerNr.text = [NSString stringWithFormat:@"%d", timer.index];

    return cell;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

@end