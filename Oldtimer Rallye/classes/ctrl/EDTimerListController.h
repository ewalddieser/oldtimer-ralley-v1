#import <Foundation/Foundation.h>
#import "VolumeButtonDelegate.h"

@class EDTimerListDataSource;


@interface EDTimerListController : UIViewController <UITableViewDelegate, UIAlertViewDelegate, VolumeButtonDelegate>

@property(strong, nonatomic) EDTimerListDataSource *dataSource;
@property(weak, nonatomic) IBOutlet UITableView *tableView;

- (IBAction)addTimer:(id)sender;

- (IBAction)deleteAll:(id)sender;

@end