#import <Foundation/Foundation.h>
#import "MZTimerLabel.h"
#import "VolumeButtonDelegate.h"

@class EDTimer;
@class RBVolumeButtons;


@interface EDRunningTimerController : UIViewController <MZTimerLabelDelegate, VolumeButtonDelegate>

@property(weak, nonatomic) IBOutlet UILabel *currentTimerLabel;
@property(weak, nonatomic) IBOutlet UIImageView *clockImage;
@property(weak, nonatomic) IBOutlet UILabel *timerLabel;
@property(weak, nonatomic) IBOutlet UILabel *stopWatchLabel;

@end