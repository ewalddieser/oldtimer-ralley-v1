#import <Foundation/Foundation.h>

@protocol VolumeButtonDelegate <NSObject>

-(void)volumeUp;
-(void)volumeDown;

@end