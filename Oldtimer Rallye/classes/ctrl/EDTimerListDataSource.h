#import <Foundation/Foundation.h>

@class EDTimer;

@interface EDTimerListDataSource : NSObject <UITableViewDataSource>

@property(nonatomic, weak) NSMutableArray *timers;

@end