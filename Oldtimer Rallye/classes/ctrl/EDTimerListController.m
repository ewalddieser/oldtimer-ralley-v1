#import <LBBlurredImage/UIImageView+LBBlurredImage.h>
#import "EDTimerListController.h"
#import "CBZSplashView.h"
#import "UIImage+PDF.h"
#import "EDTimerListDataSource.h"
#import "EDTimerEditViewController.h"
#import "EDTimer.h"
#import "NGAParallaxMotion.h"
#import "EDClicker.h"
#import "EDTimerService.h"
#import "EDVolumeClickService.h"
#import "CBZRasterSplashView.h"


@interface EDTimerListController ()

@property(weak, nonatomic) EDTimerService *timerService;
@property(strong, nonatomic) CBZSplashView *splashView;
@property(strong, nonatomic) EDClicker *clicker;

@property(strong, nonatomic) IBOutlet UIView *parallaxView;
@property(strong, nonatomic) IBOutlet UIImageView *parallaxImage;
@property(strong, nonatomic) IBOutlet UIButton *deleteButton;
@property(strong, nonatomic) IBOutlet UIButton *resetButton;
@property(strong, nonatomic) IBOutlet UIButton *addButton;
@property(strong, nonatomic) IBOutlet UIButton *infoButton;


@property(nonatomic) bool reloadNeeded;

@end

@implementation EDTimerListController {
    BOOL firstLoadDone;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    [self setupButtons];

    _dataSource = [EDTimerListDataSource new];
    self.tableView.dataSource = _dataSource;

    [self setupParallaxBG];

    [self showSplashScreen];

    _clicker = [[EDClicker alloc] init];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [UIApplication sharedApplication].idleTimerDisabled = NO;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t) (2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        if(!firstLoadDone) {
            [self.splashView startAnimation];  //FIXME should this be called every time the view appears?
            firstLoadDone = YES;
        }
    });
    if (_reloadNeeded) {
        [_tableView reloadData];
        _reloadNeeded = NO;
    }
    [EDVolumeClickService sharedInstance].delegate = self;
}


- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [EDVolumeClickService sharedInstance].delegate = nil;
}

- (void)setupParallaxBG {
    _parallaxView.parallaxIntensity = PARALLAX_INTENSITY;
    [self.parallaxImage setImageToBlur:[UIImage imageNamed:BG_IMAGE]
                            blurRadius:BLUR_RADIUS
                       completionBlock:nil];
}

- (EDTimerService *)timerService {
    if (_timerService == nil) {
        _timerService = EDTimerService.sharedInstance;
    }
    return _timerService;
}

- (void)setupButtons {
    CGFloat buttonWidth = _deleteButton.frame.size.width;
    UIImage *trashIcon = [UIImage imageWithPDFNamed:@"assets.pdf" atWidth:buttonWidth atPage:3];
    [_deleteButton setImage:trashIcon forState:UIControlStateNormal];

    UIImage *resetIcon = [UIImage imageWithPDFNamed:@"assets.pdf" atWidth:buttonWidth atPage:8];
    [_resetButton setImage:resetIcon forState:UIControlStateNormal];

    CGFloat plusWidth = _addButton.frame.size.width;
    UIImage *addIcon = [UIImage imageWithPDFNamed:@"assets.pdf" atWidth:plusWidth atPage:4];
    [_addButton setImage:addIcon forState:UIControlStateNormal];

    UIImage *infoIcon = [UIImage imageWithPDFNamed:@"assets.pdf" atWidth:buttonWidth atPage:5];
    [_infoButton setImage:infoIcon forState:UIControlStateNormal];
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}

- (void)showSplashScreen {
    float iconWidth = self.view.frame.size.width / 2;
    UIImage *icon = [UIImage imageWithPDFNamed:@"assets.pdf" atWidth:iconWidth atPage:1];

    CBZRasterSplashView *splashView = [CBZRasterSplashView alloc];
    splashView.iconStartSize = icon.size;
    splashView = [splashView initWithIconImage:icon backgroundColor:[UIColor redColor]];

    //CBZRasterSplashView *test = [CBZRasterSplashView init]

    splashView.iconStartSize = icon.size;  //FIXME this seems not to work. Bug in 1.0.0. I set the size directly in CBZSplash code
    splashView.animationDuration = 2.0;
    [self.view addSubview:splashView];
    self.splashView = splashView;
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 1) {
        [EDTimerService.sharedInstance deleteTimers];
        [_tableView reloadData];
    }
}

- (void)showEditDialog:(UIStoryboardSegue *)segue {
    EDTimerEditViewController *timerEditController = [segue destinationViewController];
    NSUInteger selectedRow = (NSUInteger) [self.tableView indexPathForSelectedRow].row;
    EDTimer *timer = _dataSource.timers[selectedRow];
    timerEditController.timer = timer;
    _reloadNeeded = YES;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"showEditDialog"]) {
        [self showEditDialog:segue];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        return 88;
    }
    return 44;
}

- (IBAction)start:(id)sender {
    if (![self.timerService start]) {
        [_clicker zeroClick];
        return;
    }
    [_clicker click];
    _reloadNeeded = YES;
    [self performSegueWithIdentifier:@"startTimer" sender:self];
}

- (IBAction)addTimer:(id)sender {
    [EDTimerService.sharedInstance resetTimers];
    [_tableView reloadData];

    [self.timerService addTimerAndCall:^{
        [self.tableView beginUpdates];
        NSIndexPath *pathToInsert = [NSIndexPath indexPathForRow:_dataSource.timers.count - 1 inSection:0];
        [self.tableView insertRowsAtIndexPaths:@[pathToInsert]
                              withRowAnimation:UITableViewRowAnimationRight];
        [self.tableView endUpdates];
        [self.tableView scrollToRowAtIndexPath:pathToInsert
                              atScrollPosition:UITableViewScrollPositionBottom
                                      animated:YES];
    }];
}

- (IBAction)deleteAll:(id)sender {
    UIAlertView *alert = [[UIAlertView alloc]
            initWithTitle:@"Alles löschen?"
                  message:@""
                 delegate:self
        cancelButtonTitle:@"Abbrechen"
        otherButtonTitles:@"OK", nil];
    [alert show];
}

- (IBAction)resetTimers:(id)sender {
    [EDTimerService.sharedInstance resetTimers];
    [_tableView reloadData];
}

- (void)volumeUp {
    [self start:nil];
}

- (void)volumeDown {
    [self start:nil];
}

@end