#import <Foundation/Foundation.h>

@class EDTimer;

@interface EDTimerEditViewController : UIViewController

@property(weak, nonatomic) EDTimer *timer;

@property(strong, nonatomic) IBOutlet UIImageView *parallaxImage;

@end