#import "EDTimerEditViewController.h"
#import "AKPickerView.h"
#import "EDTimer.h"
#import "NGAParallaxMotion.h"

#import "UIImageView+LBBlurredImage.h"
#import "EDTimerService.h"
#import "UIImage+PDF.h"
#import "EDPickerDataSource.h"

@interface EDTimerEditViewController () <AKPickerViewDelegate>
@property(strong, nonatomic) IBOutlet UILabel *timeLabel;
@property(strong, nonatomic) IBOutlet UIView *hoursView;
@property(strong, nonatomic) IBOutlet UIView *minutesView;
@property(strong, nonatomic) IBOutlet UIView *secondsView;
@property(strong, nonatomic) IBOutlet UIView *selectionView;
@property(strong, nonatomic) IBOutlet UIView *parallaxView;
@property(strong, nonatomic) IBOutlet UIImageView *timerIcon;
@property(strong, nonatomic) IBOutlet UILabel *timerIndexLabel;

@property(nonatomic, strong) AKPickerView *hoursPicker;
@property(nonatomic, strong) AKPickerView *minutesPicker;
@property(nonatomic, strong) AKPickerView *secondsPicker;

@property(nonatomic, strong) EDPickerDataSource *pickerDataSource;

@end

@implementation EDTimerEditViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.pickerDataSource = [EDPickerDataSource new];

    _selectionView.backgroundColor = [UIColor clearColor];
    _parallaxView.parallaxIntensity = PARALLAX_INTENSITY;
    self.parallaxImage.image = [UIImage imageNamed:BG_IMAGE];  //to avoid flickering on slower devices
    [self.parallaxImage setImageToBlur:[UIImage imageNamed:BG_IMAGE]
                            blurRadius:BLUR_RADIUS
                       completionBlock:nil];
    self.timerIndexLabel.text = [NSString stringWithFormat:@"%d", self.timer.index];
    [self setupClockIcon];
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}

- (void)setupClockIcon {
    CGFloat clockWidth = _timerIcon.frame.size.width;
    UIImage *clockIcon = [UIImage imageWithPDFNamed:@"assets.pdf" atWidth:clockWidth atPage:2];
    _timerIcon.image = clockIcon;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self setupPickersViews];
    [self updatePickerSelections];
    [self initSelectionBorder];
}

- (void)setupPickersViews {
    self.hoursPicker = [self createPickerInView:self.hoursView];
    self.hoursPicker.tag = 666;
    self.minutesPicker = [self createPickerInView:self.minutesView];
    self.secondsPicker = [self createPickerInView:self.secondsView];
}

/**
* Draws a box with rounded corners around the selected values
*/
- (void)initSelectionBorder {
    [_selectionView.layer setCornerRadius:15.0f];
    [_selectionView.layer setBorderColor:[UIColor colorWithWhite:1 alpha:0.4].CGColor];
    [_selectionView.layer setBorderWidth:1.0f];
}

- (void)updatePickerSelections {
    unsigned int hours = (unsigned int) floor(_timer.timeInterval / (60 * 60));
    unsigned int minutes = (unsigned int) floor((_timer.timeInterval / 60) - hours * 60);
    unsigned int seconds = (unsigned int) floor(_timer.timeInterval - (minutes * 60) - (hours * 60 * 60));
    [_hoursPicker selectItem:hours animated:YES];
    [_minutesPicker selectItem:minutes animated:YES];
    [_secondsPicker selectItem:seconds animated:YES];
}

- (AKPickerView *)createPickerInView:(UIView *)view {
    AKPickerView *picker = [[AKPickerView alloc] initWithFrame:view.bounds];
    picker.delegate = self;
    picker.dataSource = self.pickerDataSource;
    picker.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [view addSubview:picker];

    picker.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:66];
    picker.highlightedFont = [UIFont fontWithName:@"HelveticaNeue" size:66];
    picker.highlightedTextColor = [UIColor whiteColor];
    picker.textColor = [UIColor colorWithWhite:0.5 alpha:1];
    picker.interitemSpacing = 5.0;
    picker.fisheyeFactor = 0.001;
    picker.pickerViewStyle = AKPickerViewStyleFlat;

    [picker reloadData];
    return picker;
}

- (void)pickerView:(AKPickerView *)pickerView didSelectItem:(NSInteger)item {
    self.timer.timeInterval = _hoursPicker.selectedItem * 3600 + _minutesPicker.selectedItem * 60 + _secondsPicker.selectedItem;
    self.timeLabel.text = _timer.strVal;
}

- (IBAction)ok:(id)sender {
    [EDTimerService.sharedInstance persistTimers];
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end