#import "EDRunningTimerController.h"
#import "EDTimer.h"
#import "UIImage+PDF.h"
#import "EDClicker.h"
#import "EDTimerService.h"
#import "EDVolumeClickService.h"

@interface EDRunningTimerController ()
@property(strong, nonatomic) EDClicker *clicker;
@property(weak, nonatomic) EDTimerService *timerService;
@property(strong, nonatomic) MZTimerLabel *timerView;
@property(strong, nonatomic) MZTimerLabel *stopWatchView;
@property(weak, nonatomic) IBOutlet UIButton *revertButton;
@end

@implementation EDRunningTimerController {
    int lastClick;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupAndStartTimer];
    lastClick = 10;
    _stopWatchLabel.hidden = YES;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    _clicker = [[EDClicker alloc] init];
    [self setupOvertimeStopWatch];

    [self setupClockIcon];
    [self setupRevertButton];

    [self updateTimerNr];

    [EDVolumeClickService sharedInstance].delegate = self;

    [UIApplication sharedApplication].idleTimerDisabled = YES;
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [EDVolumeClickService sharedInstance].delegate = nil;
    [UIApplication sharedApplication].idleTimerDisabled = NO;
}

- (EDTimerService *)timerService {
    if (_timerService == nil) {
        _timerService = EDTimerService.sharedInstance;
    }
    return _timerService;
}

- (void)setupClockIcon {
    CGFloat clockWidth = _clockImage.frame.size.width;
    UIImage *clockIcon = [UIImage imageWithPDFNamed:@"assets.pdf" atWidth:clockWidth atPage:2];
    _clockImage.image = clockIcon;
}

- (void)setupRevertButton {
    CGFloat revertButtonWidth = _revertButton.frame.size.width;
    UIImage *revertButtonIcon = [UIImage imageWithPDFNamed:@"assets.pdf" atWidth:revertButtonWidth atPage:9];
    [_revertButton setImage:revertButtonIcon forState:UIControlStateNormal];
    _revertButton.hidden = [self isRevertButtonHidden];
}

- (BOOL)isRevertButtonHidden {
    return !self.timerService.revertable;
}

- (void)updateTimerNr {
    unsigned long timerNr = self.timerService.currentTimerIndex + 1;
    _currentTimerLabel.text = [NSString stringWithFormat:@"%lu", timerNr];
}

- (void)setupAndStartTimer {
    _timerView = [[MZTimerLabel alloc]
            initWithLabel:_timerLabel
             andTimerType:MZTimerLabelTypeTimer];
    _timerView.timeFormat = @"HH:mm:ss,S";  // @"HH:mm:ss,SS";
    _timerView.countDownTime = self.timerService.currentTimer.timeInterval;
    _timerView.delegate = self;
    [_timerView start];
}

- (void)setupOvertimeStopWatch {
    _stopWatchLabel.text = @"";
    _stopWatchView = [[MZTimerLabel alloc]
            initWithLabel:_stopWatchLabel
             andTimerType:MZTimerLabelTypeStopWatch];
    [_stopWatchView setStopWatchTime:0];
    _stopWatchView.timeFormat = @"HH:mm:ss,S"; // @"HH:mm:ss,SS";
}


- (void)timerLabel:(MZTimerLabel *)timerLabel finshedCountDownTimerWithTime:(NSTimeInterval)countTime {
    [_clicker zeroClick];
    [self startOverTime];
}

- (void)startOverTime {
    [self startOverTimeWithInitialValue:0];
}

- (void)startOverTimeWithInitialValue:(NSTimeInterval)intitialValue {
    [_stopWatchView reset];
    if (intitialValue != 0) {
        [_stopWatchView setStopWatchTime:intitialValue];
    }
    _timerLabel.hidden = YES;
    _stopWatchLabel.hidden = NO;
    [_stopWatchView start];
}

- (void)timerLabel:(MZTimerLabel *)timerLabel
        countingTo:(NSTimeInterval)time
         timertype:(MZTimerLabelType)timerType {

    if (time > lastClick) return; //this is before the 10sec count down
    int intTime = (int) time;
    if (intTime < lastClick) {
        [_clicker click];
        lastClick = lastClick - 1;
    }
}

/**
* Will be called when the last timer was stopped by accident.
*/
- (IBAction)revertTimer:(id)sender {
    [self.timerService revertAndCall:^(int timerIndex, NSTimeInterval deltaInterval) {
        [_timerView reset];
        [_timerView pause];
        [_stopWatchView reset];
        [_stopWatchView pause];
        _stopWatchLabel.hidden = YES;
        _timerLabel.hidden = YES;

        if (deltaInterval < 0) {
            _timerView.countDownTime = deltaInterval * (-1);
            lastClick = (int) deltaInterval * (-1);
            if (lastClick > 10) lastClick = 10;
            _timerLabel.hidden = NO;
            [_timerView start];
        } else {
            [self startOverTimeWithInitialValue:deltaInterval];
        }
        [self updateTimerNr];
    }];
    _revertButton.hidden = [self isRevertButtonHidden];
}

- (IBAction)stop:(id)sender {
    [_timerView reset];
    [_timerView pause];

    lastClick = 10;

    if (![self.timerService startNext]) {  // done, all timers stopped
        [self dismissViewControllerAnimated:YES completion:nil];
        return;
    }

    _timerView.countDownTime = self.timerService.currentTimer.timeInterval;
    [_timerView start];

    _stopWatchLabel.hidden = YES;
    _timerLabel.hidden = NO;
    [_stopWatchView reset];

    _revertButton.hidden = [self isRevertButtonHidden];

    [self updateTimerNr];
}

- (BOOL)prefersStatusBarHidden {
    return NO;
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

- (void)volumeUp {
    [self stop:nil];
}

- (void)volumeDown {
    [self stop:nil];
}

@end