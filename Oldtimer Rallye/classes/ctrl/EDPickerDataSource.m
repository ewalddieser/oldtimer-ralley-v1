#import "EDPickerDataSource.h"
#import "EDTimer.h"


@interface EDPickerDataSource ()
@property(nonatomic, strong) NSArray *hours;
@property(nonatomic, strong) NSArray *minutesSeconds;
@end


@implementation EDPickerDataSource {

}

- (instancetype)init {
    self = [super init];
    if (self) {
        [self setupPickerValues];
    }
    return self;
}

- (void)setupPickerValues {
    NSMutableArray *hours = [NSMutableArray new];
    for (int i = 0; i <= 23; i++) {
        [hours addObject:[NSString stringWithFormat:@"%d", i]];
    }
    _hours = [NSArray arrayWithArray:hours];

    NSMutableArray *minutesSeconds = [NSMutableArray new];
    for (int i = 0; i <= 59; i++) {
        [minutesSeconds addObject:[NSString stringWithFormat:@"%d", i]];
    }
    _minutesSeconds = [NSArray arrayWithArray:minutesSeconds];
}

#pragma Delegate stuff


- (NSUInteger)numberOfItemsInPickerView:(AKPickerView *)pickerView {
    if (pickerView.tag == 666) {  //FIXME!!! stupid solution!
        return [self.hours count];
    }
    return [self.minutesSeconds count];
}

- (NSString *)pickerView:(AKPickerView *)pickerView titleForItem:(NSInteger)item {
    return [EDTimer strVal:[self.minutesSeconds[item] intValue]];
}

@end