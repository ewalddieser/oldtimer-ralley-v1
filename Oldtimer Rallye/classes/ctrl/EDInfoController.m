#import <LBBlurredImage/UIImageView+LBBlurredImage.h>
#import "EDInfoController.h"
#import "UIImage+PDF.h"
#import "NGAParallaxMotion.h"

@interface EDInfoController ()
@property(strong, nonatomic) IBOutlet UIImageView *parallaxImage;
@property(strong, nonatomic) IBOutlet UIView *parallaxView;
@property(strong, nonatomic) IBOutlet UIImageView *logo;
@property(strong, nonatomic) IBOutlet UIButton *closeButton;
@end

@implementation EDInfoController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupParallaxBG];
    [self setupLogo];
    [self setupCloseIcon];
}

- (void)setupLogo {
    CGFloat logoWidth = _logo.frame.size.width;
    UIImage *addIcon = [UIImage imageWithPDFNamed:@"assets.pdf" atWidth:logoWidth atPage:7];
    _logo.image = addIcon;
}

- (void)setupCloseIcon {
    UIImage *closeIcon;
    closeIcon = [UIImage imageWithPDFNamed:@"assets.pdf"
                                   atWidth:_closeButton.frame.size.width
                                    atPage:10];

    [_closeButton setImage:closeIcon forState:UIControlStateNormal];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}

- (void)setupParallaxBG {
    self.parallaxImage.image = [UIImage imageNamed:BG_IMAGE2];  //to avoid flickering on slower devices
    _parallaxView.parallaxIntensity = PARALLAX_INTENSITY;
    [self.parallaxImage setImageToBlur:self.parallaxImage.image
                            blurRadius:BLUR_RADIUS
                       completionBlock:nil];
}

- (IBAction)backAction:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end