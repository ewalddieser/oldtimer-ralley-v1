#import <UIImage+PDF/UIImage+PDF.h>
#import "EDTimerCell.h"

@implementation EDTimerCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self addBorderToTimerIndex];
    }
    return self;
}

- (void)awakeFromNib {
    [self addBorderToTimerIndex];
}

- (void)addBorderToTimerIndex {
    CGFloat clockWidth = _countBorder.frame.size.width;
    UIImage *clockIcon = [UIImage imageWithPDFNamed:@"assets.pdf" atWidth:clockWidth atPage:2];
    _countBorder.image = clockIcon;

    CGFloat disclosureWidth = _disclosureInticator.frame.size.width;
    UIImage *disclosureIcon = [UIImage imageWithPDFNamed:@"assets.pdf" atHeight:disclosureWidth atPage:6];
    _disclosureInticator.image = disclosureIcon;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

}

@end