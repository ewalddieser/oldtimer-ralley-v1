#import <UIKit/UIKit.h>

@interface EDTimerCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *countBorder;
@property (strong, nonatomic) IBOutlet UIImageView *disclosureInticator;

@end