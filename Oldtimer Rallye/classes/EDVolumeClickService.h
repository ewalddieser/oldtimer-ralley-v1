#import <Foundation/Foundation.h>

@protocol VolumeButtonDelegate;


@interface EDVolumeClickService : NSObject

@property(weak, nonatomic) id <VolumeButtonDelegate> delegate;


+ (EDVolumeClickService *)sharedInstance;


@end